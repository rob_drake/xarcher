var xArcher = xArcher || {};

xArcher.game = new Phaser.Game(512, 512, Phaser.AUTO, 'gameDiv');
xArcher.game.state.add('boot', xArcher.boot);
xArcher.game.state.add('load', xArcher.load);
xArcher.game.state.add('play', xArcher.play);

xArcher.game.state.start('boot');