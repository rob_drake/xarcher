var xArcher = xArcher || {};

xArcher.load = function(){};

xArcher.load.prototype = {
    preload: function() {
        this.game.load.image('background', 'assets/images/background.png');
    },
    create: function() {
        this.game.state.start('play');
    }
};