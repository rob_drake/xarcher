var xArcher = xArcher || {};

xArcher.boot = function() {};
xArcher.boot.prototype = {
    preload: function() {
        
    },
    create: function() {
        this.game.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVertically = true;
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.state.start('load');
    }
};