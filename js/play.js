var xArcher = xArcher || {};

xArcher.play = function() {};

xArcher.play.prototype = {
    preload: function() {
        this.game.time.advancedTiming = true;
    },
    
    create: function() {
        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.background = this.game.add.sprite(0, 0, 'background');
        this.player = this.game.add.sprite(20, 480 - 22, 'figure');
        this.bow = this.game.add.sprite(this.player.position.x + 16, this.player.position.y + 16, 'bow');
        this.bow.anchor.setTo(0, 0.5);
    },
    update: function() {
        if(this.cursors.up.isDown) {
            this.bow.angle -=2;
        }
        if(this.cursors.down.isDown) {
            this.bow.angle += 2;
        }
  
    }
};